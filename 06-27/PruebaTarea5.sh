#!/bin/bash
#Definimos variables
FECHAHORA=$(date +%d-%m-%Y-%H.%M)
BACKUP=$HOME/backup/
TEMPUSER=/tmp/$USER/
LOG=/tmp/$USER/backup.log
NOMBREARCH=BackUp-$USER_${FECHAHORA}.tar.gz
DIRECTORIO=$1

#Creamos directorio y archivos
mkdir -p $BACKUP && mkdir -p $TEMPUSER && touch $LOG && echo "Creados directorios y fichero de logs: $(date +%d-%m-%Y-%H.%M)" 1>>$LOG 2>>$LOG

#Pedimos ruta del directorio a backupear
echo "Hola, este script realizará un backup de un directorio definido por Ud, 2 veces al día";
echo "";
echo "Para mejor funcionamiento del script, por favor sitúelo en su directorio $HOME";
echo "";
read -p "Indicar la ruta absoluta del directorio que desea respaldar:" DIRECTORIO

#Chequeamos que haya ingresado lo solicitado
if [ -z "${DIRECTORIO}" ] ; then
        echo "";
        echo "Debe ingresar una ruta hacia el directorio a respaldar";
        echo "";
        exit 1
fi

#Validamos existencia del directorio
if [ ! -d ${DIRECTORIO} ];then
	echo "Debe ingresar una ruta hacia su directorio existente";
	echo "";
	exit 1
fi

#Corroboramos que no exista previamente el archivo comprimido, caso contrario procedemos a comprimir.
if [ -f {$NOMBREARCH} ]; then
	echo "El archivo $NOMBREARCH ya existe";
else
	tar -cvzf ${BACKUP}/${NOMBREARCH} ${DIRECTORIO} 1>>$LOG 2>>$LOG |& tee -a $LOG
	echo "Compresión con tar hecho: $(date +%d-%m-%Y-%H.%M)" 1>>$LOG 2>>$LOG |& tee -a $LOG
fi

#Chequeamos que exista el archivo con llave pública
if [ ! -f $HOME/.ssh/id_rsa.pub ]; then
	echo "Debe generar su llave SSH, presionar enter y yes cuando se requiera :"
	ssh-keygen -t rsa
	ssh-copy-id -i ~/.ssh/id_rsa.pub $USER@localhost
fi

#Realizamos la sincronización "remota"
rsync -a $BACKUP $USER@localhost:$TEMPUSER 1>>$LOG 2>>$LOG |& tee -a $LOG
echo "Fecha y hora de sincronizacion del backup: $(date +%d-%m-%Y-%H.%M)" 1>>$LOG 2>>$LOG |& tee -a $LOG

#Hago un respaldo del cron actual y agrego setteo horario de cron al archivo del usuario
crontab -l > cron_$USER 1>>$LOG 2>>$LOG
echo " 0 3,19 * * * $HOME/PruebaTarea5.sh" >> cron_$USER

#Cargo el archivo en el crontab del usuario que esta ejecutando el programa
crontab -u $USER cron_$USER

#Automatizamos la tarea con servicio cron a las 3 y a las 19 hs.
#echo "0 3,19 * * * $HOME/PruebaTarea5.sh" >> /var/spool/cron/crontabs/$USER

echo "Crontab agregado: $(date +%d-%m-%Y-%H.%M)" 1>>$LOG 2>>$LOG |& tee -a $LOG
echo "Proceso culminado, puede ver registros en $LOG"
