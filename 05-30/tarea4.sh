#!/bin/bash
args=( $@ )

#Declaramos la función que mostrará la ayuda del script
Ayuda(){
echo "Se mostrarán instrucciones sobre cómo ejecutar este script:";
sleep 2s
cat MAN.txt
exit 1
}


#Declaramos la función que validará el formato de nro. entero mayor que cero.
NroEnt(){
NumEnt='^[1-9]+[0-9]*$'
                if ! [[ $cantidad =~ $NumEnt ]] ; then
                        echo "";
                        echo "Debe ingresar un numero entero positivo mayor que cero luego de la opción -C";
                        sleep 1s
                        Ayuda
                else
                        counter="-c $cantidad"
                fi
}


#Declaro la función que validará el protocolo
ValidProto(){
#Me fijo que exista un 4 ó 6 como argumento de la opción -p, sino marco el yerro y doy instrucciones de ayuda
                if [ "$proto" -eq 4 -o "$proto" -eq 6 ] ;then
                        p="-$proto"
                else
                        echo "El valor siguiente al -p debe ser igual a 4 ó 6.";
                        echo "";
                        Ayuda
                fi
}


#Declaro la variable que traerá el valor del último argumento, usaré dicha variable para realizar validaciones
Dest=${args[-1]}

#Validamos que se ingresen por lo menos 2 opciones
if [ "$#" -lt "2" ] ; then
        echo "";
        echo "Debe ingresar por lo menos 2 opciones";
        echo "";
        Ayuda
fi

#Declaro 4 variables que me ayudarán a representar los 4 octetos de una IP
Oct1=`echo "$Dest"|cut -d "." -f1`
Oct2=`echo "$Dest"|cut -d "." -f2`
Oct3=`echo "$Dest"|cut -d "." -f3`
Oct4=`echo "$Dest"|cut -d "." -f4`


#Declaro la función que me ayudará a validar el formato del último argumento, si es una IP o hostname
ValidUltArg(){
#Valido los octetos
if [[ $Dest =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ && $Oct1 -ge 0 && $Oct1 -le 255 && $Oct2 -ge 0 && $Oct2 -le 255 && $Oct3 -ge 0 && $Oct3 -le 255 && $Oct4 -ge 0 && $Oct4 -le 255 ]]; then
        echo "Formato IP validado, si todo está bien, se procederá con la ejecución:";
        echo "";
else
#Sino, valido si tiene formato de host separado por lo menos por 1 punto
        if [[ ${Dest} =~ ^[a-zA-Z-].+\.[a-zA-Z-].+$ ]] || [[ ${Dest} =~ ^[a-zA-Z-].+\.[a-zA-Z-].+\.[a-zA-Z-].+$ ]] || [[ ${Dest} =~ ^[a-zA-Z-].+\.[a-zA-Z-].+\.[a-zA-Z-].+\.[a-zA-Z-].+$ ]] || [[ ${Dest} =~ ^[a-zA-Z-].+\.[a-zA-Z-].+\.[a-zA-Z-].+\.[a-zA-Z-].+\.[a-zA-Z-].+$ ]] || [[ ${Dest} =~ ^[a-zA-Z-].+\.[a-zA-Z-].+\.[a-zA-Z-].+\.[a-zA-Z-].+\.[a-zA-Z-].+\.[a-zA-Z-].+$ ]]; then
                echo "Formato Host validado";
                echo "";
                echo "Si todo está OK, se procederá con la ejecución:";
                echo "";
        else
#Si no tiene formato IP o host, devuelvo error, instrucciones y salida
                echo "Debe ingresar una IP, dominio o host, en formato válido";
                echo "";
                Ayuda
        fi
fi
}

#Luego, validar el último argumento
ValidUltArg

for opcion in "${!args[@]}"; do
case ${args[$opcion]} in
        -C) cantidad=${args[$(($opcion+1))]}
			NroEnt
                ;;
        -T) timestamp="-D"
                ;;
        -p) proto=${args[$(($opcion+1))]}
			ValidProto
                ;;
        -b) b="-b"
                ;;
        [0-9])
                ;;
        ${args[-1]})
                ;;
        *) echo "Por favor ingresar opciones válidas";
                echo "";
                Ayuda
                ;;
esac
done

ping $b $timestamp $p $counter ${args[-1]}
