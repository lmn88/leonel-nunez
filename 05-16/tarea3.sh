#!/bin/bash
args=( $@ )

#Validamos que se ingresen por lo menos 2 opciones
if [ "$#" -lt "2" ] ; then
        echo "";
        echo "Debe ingresar por lo menos 2 opciones";
        echo "";
        echo "Se mostrarán instrucciones sobre cómo ejecutar este script:";
        sleep 2s
        cat MAN.txt
        exit 1
fi

#Declaro la variable que traerá el valor del último argumento, usaré dicha variable para validar el formato de la misma
Dest=${args[-1]}

#Separo la variable declarando 4 variables que representarán los 4 octetos
Oct1=`echo "$Dest"|xargs|cut -d "." -f1`
Oct2=`echo "$Dest"|xargs|cut -d "." -f2`
Oct3=`echo "$Dest"|xargs|cut -d "." -f3`
Oct4=`echo "$Dest"|xargs|cut -d "." -f4`

#Valido los octetos
if [[ $Dest =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ && $Oct1 -ge 0 && $Oct1 -le 255 && $Oct2 -ge 0 && $Oct2 -le 255 && $Oct3 -ge 0 && $Oct3 -le 255 && $Oct4 -ge 0 && $Oct4 -le 255 ]]; then
        echo "Formato IP validado, si todo está bien, se procederá con la ejecución:";
        echo "";
else
#Valido si tiene formato de host con letras y separado por lo menos por 1 punto
        if [[ ${Dest} =~ ^[a-z].+\.[a-z].+$ ]] || [[ ${Dest} =~ ^[a-z].+\.[a-z].+\.[a-z].+$ ]] || [[ ${Dest} =~ ^[a-z].+\.[a-z].+\.[a-z].+\.[a-z].+$ ]] || [[ ${Dest} =~ ^[a-z].+\.[a-z].+\.[a-z].+\.[a-z].+\.[a-z].+$ ]] || [[ ${Dest} =~ ^[a-z].+\.[a-z].+\.[a-z].+\.[a-z].+\.[a-z].+\.[a-z].+$ ]]; then
                echo "Formato Host validado";
                echo "";
                echo "Si todo está OK, se procederá con la ejecución:";
                echo "";
        else
#Si no tiene formato IP o host, devuelvo error, instrucciones y salida
                echo "Debe ingresar una IP, dominio o host, en formato válido";
                echo "";
                echo "Se mostrarán instrucciones sobre cómo ejecutar este script:";
                sleep 2s
                cat MAN.txt
                exit 1
        fi
fi

for opcion in "${!args[@]}"; do
case ${args[$opcion]} in
        -C) cantidad=${args[$(($opcion+1))]}
#Valido que sea un nro. entero declarando una variable para compararla, sino doy ayuda y salida del programa.
                NumEnt='^[0-9]+$'
                if ! [[ $cantidad =~ $NumEnt ]]; then
                        echo "";
                        echo "Debe ingresar un numero entero positivo luego de la opción -C";
                        sleep 1s
                        echo "";
                        echo "Se mostrarán instrucciones sobre cómo ejecutar este script:";
                        sleep 2s
                        cat MAN.txt
                        exit 1
                else
                        counter="-c $cantidad"
                fi
                ;;
        -T) timestamp="-D"
                ;;
        -p) proto=${args[$(($opcion+1))]}
#Valido que exista un 4 ó 6 como argumento de la opción -p, sino marco el yerro, doy instrucciones de ayuda y salida.
                if [ "$proto" -eq 4 ] ;then
                        p="-$proto"
                else
                        if [ "$proto" -eq 6 ] ;then
                                p="-$proto"
                        else
                                echo "El valor siguiente al -p debe ser igual a 4 ó 6.";
                                echo "";
                                echo "Se mostrarán instrucciones sobre cómo ejecutar este script:";
                                sleep 2s
                                cat MAN.txt
                                exit 1
                        fi
                fi
                ;;
        -b) b="-b"
                ;;
esac
done
ping $b $timestamp $p $counter ${args[-1]}

