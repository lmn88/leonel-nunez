

INSTRUCCIONES

TAREA 3

Debe ejecutar el script teniendo en cuenta:

*Cuando ingrese la opción -C , debe ingresar como argumento un nro. entero positivo.

*Para la opción -p, la misma debe ser nro 4 o nro 6 si tuviese IPv6 habilitado.

*El argumento host/IP debe ser en formato de direccion IP (192.168.0.1 por ej), puede ser en formato de hostname (NOTEBOOK-VENTAS-01) o también en formato dominio (google.com), no se aceptan otros formatos como URL (https://www.google.com/). El script arrojará un error si se omite este argumento.

*El script debe recibir al menos una opción ademas del argumento anterior, es decir, mínimo 2 opciones. Además, el script arrojará error si se pasan opciones incorrectas o inexistentes.

*No importa el orden de las opciones del comando ping, pero recuerde que el destino IP, dominio o hostname, debe ir al final.

Si no está familiarizado con el comando ping, para más detalles, puede ejecutar man ping



