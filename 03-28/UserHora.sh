#!/bin/bash

#Determino una variable que me dé la hora tal como quiero y la exporto
HORA=$(date '+%T')
export HORA=$(date '+%T')

#Imprimo el mensaje de saludo con la variable de la hora
echo "Hola $USER la hora en este momento es $HORA"
sleep 1s

#Declaro una variable que me dé el ultimo login e IP del usuario en uso y la exporto
ULTLOGIN=$(lastlog -u $USER)
export ULTLOGIN=$(lastlog -u $USER)

#
echo "Su ultimo login (con fecha, hora e IP) fue:"
echo "$ULTLOGIN"

#EOF
