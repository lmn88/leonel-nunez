#!/bin/bash
# Declaramos variables
BACKUP=$HOME/backup/
LOG=/tmp/$USER/backup.log
TEMPUSER=/tmp/$USER/

# Creamos directorio y archivos
mkdir -p $BACKUP && mkdir -p $TEMPUSER && touch $LOG && echo "Creados directorio y fichero: $(date '+%T')" >> $LOG && \

# Hacemos backup
tar -c -v -z -f $USER.$(date +%b.%e.%Y.%H.%M.%S).tar.gz $HOME &>$LOG
echo "Compresión con tar hecho: $(date '+%T')" >> $LOG

# Movemos el archivo
mv -i $USER.$(date +%b.%e.%Y.%H.%M.%S).tar.gz /$BACKUP && echo "Comando mv realizado: $(date '+%T')" >> $LOG
echo "{Fecha y hora de finalizacion del backup: $(date)}" >>$LOG

#Validamos el archivo con llave pública
if [ ! -f $HOME/.ssh/id_rsa.pub ]; then
        echo "Debe generar su llave SSH, presionar enter 3 veces:"
        ssh-keygen -t rsa
        ssh-copy-id -i ~/.ssh/id_rsa.pub $USER@localhost
fi

#Sincronizaremos el directorio /tmp/$USER/ con el /$HOME/backup/"
rsync -a /tmp/$USER/ $USER@localhost:/$HOME/backup/

#Cron para automatizar el script a las 00:00 hs de cada día
(crontab -l; echo "0 0 * * * /home/$USER/PruebaBU5.sh") | sort -u | crontab -
