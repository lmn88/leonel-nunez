#!/bin/bash
#Declaracion del intérprete
#Pediremos que se ingrese con permisos root
echo "El siguiente script debe ser ejecutado con permisos de root"
echo " "
read -p "Si eso está OK, presione Enter. Sino, presione Ctrl+c y reingrese como root"
#Validaremos esa condición
if [ $UID -eq 0 ]; then
	echo " "
	echo "Validando su elección..."
	echo " "
	read -p "Validación de root OK, presione enter para proseguir"
else
	echo " "
	echo "Validando su elección..."
	echo " "
	echo "No cuenta con permisos root, reingrese como root"
	exit 1
fi
#Evaluaremos si el fichero existe
echo " "
read -p "Por favor ingrese la ruta completa del fichero que desea chequear:" fichero
echo " "
if [ -e "$fichero" ]; then
	echo "$fichero existe"
	echo " "
else
	echo "$fichero no existe"
	exit 1
fi
#Evaluaremos si el fichero tiene permisos de ejecución por el usuario actual
echo "Verificaremos permisos de ejecución"
echo " "
if [ -x "$fichero" ]; then
	echo "Ud tiene permisos de ejecución, se mostrará información del fichero:"
	echo " "
	ls -l $fichero
	echo " "
	read -p "Desea ejecutar el fichero? Ingresar s para sí, sino salga con cualquier tecla:" RPTA
	if [[ $RPTA =~ [sS] ]]; then
		echo "Ejecutaremos el fichero $fichero"
		echo " "
		echo " "
		echo "Fichero ejecutado en 2do plano. Hasta luego"
	else
		echo "¡Hasta luego!"
		exit 1
	fi
else
	if [ -O "$fichero" ]; then
		echo "Ud no tiene permisos de ejecución, pero es el owner del $fichero"
		read -p "Desea permisos de ejecución sobre el fichero? Ingresar s para sí, sino salga con ctrl+c:" RTA
		if [[ $RTA =~ [sS] ]]; then
			chmod +x $fichero
			echo " "
			echo "$fichero ahora cuenta con dicho permiso:" ; ls -l $fichero
		else
			echo "¡Hasta luego!"
			exit 1
		fi
	else
		echo "Ud no tiene permisos de ejecución y tampoco es owner del $fichero"
		echo " "
		echo "Hasta luego"
	fi
fi
