#!/bin/bash
args=( $@ )
source ./Funciones.sh

#Declaro la variable que traerá el valor del último argumento, usaré dicha variable para realizar validaciones
Dest=${args[-1]}

#Validamos que se ingresen por lo menos 2 opciones
if [ "$#" -lt "2" ] ; then
        echo "";
        echo "Debe ingresar por lo menos 2 opciones";
        echo "";
        Ayuda
        exit 1
fi


#Declaro 4 variables que me ayudarán a representar los 4 octetos de una IP
Oct1=`echo "$Dest"|cut -d "." -f1`
Oct2=`echo "$Dest"|cut -d "." -f2`
Oct3=`echo "$Dest"|cut -d "." -f3`
Oct4=`echo "$Dest"|cut -d "." -f4`


#Luego, validar el último argumento
ValidUltArg


for opcion in "${!args[@]}"; do
case ${args[$opcion]} in
        -C) cantidad=${args[$(($opcion+1))]}
                        NroEnt
                ;;
        -T) timestamp="-D"
                ;;
        -p) proto=${args[$(($opcion+1))]}
                        ValidProto
                ;;
        -b) b="-b"
                ;;
        *) echo "Por favor ingresar opciones válidas";
		echo "";
                Ayuda
                exit 1
                ;;
esac
done

ping $b $timestamp $p $counter ${args[-1]}
