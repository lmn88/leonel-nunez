#!/bin/bash
#Declaramos la función que mostrará la ayuda del script

Ayuda(){
echo "Se mostrarán instrucciones sobre cómo ejecutar este script:";
sleep 2s
cat MAN.txt
return 1
}


#Declaramos la función que validará el formato de nro. entero mayor que cero.
NroEnt(){
NumEnt='^[1-9]+[0-9]*$'
                if ! [[ $cantidad =~ $NumEnt ]] ; then
                        echo "";
                        echo "Debe ingresar un numero entero positivo mayor que cero luego de la opción -C";
                        sleep 1s
                        Ayuda
                        exit 1
                else
                        counter="-c $cantidad"
                fi
}


#Declaro la función que validará el protocolo
ValidProto(){
#Me fijo que exista un 4 ó 6 como argumento de la opción -p, sino marco el yerro y doy instrucciones de ayuda
                if [ "$proto" -eq 4 -o "$proto" -eq 6 ] ;then
                        p="-$proto"
                else
                        echo "El valor siguiente al -p debe ser igual a 4 ó 6.";
                        echo "";
                        Ayuda
                        exit 1
                fi
}


#Declaro la función que me ayudará a validar el formato del último argumento, si es una IP o hostname
ValidUltArg(){
#Valido los octetos
if [[ $Dest =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ && $Oct1 -ge 0 && $Oct1 -le 255 && $Oct2 -ge 0 && $Oct2 -le 255 && $Oct3 -ge 0 && $Oct3 -le 255 && $Oct4 -ge 0 && $Oct4 -le 255 ]]; then
        echo "Formato IP validado, si todo está bien, se procederá con la ejecución:";
        return 0
        echo "";
else
#Sino, valido si tiene formato de host separado por lo menos por 1 punto
        if [[ ${Dest} =~ ^[a-zA-Z-].+\.[a-zA-Z-].+$ ]] || [[ ${Dest} =~ ^[a-zA-Z-].+\.[a-zA-Z-].+\.[a-zA-Z-].+$ ]] || [[ ${Dest} =~ ^[a-zA-Z-].+\.[a-zA-Z-].+\.[a-zA-Z-].+\.[a-zA-Z-].+$ ]] || [[ ${Dest} =~ ^[a-zA-Z-].+\.[a-zA-Z-].+\.[a-zA-Z-].+\.[a-zA-Z-].+\.[a-zA-Z-].+$ ]] || [[ ${Dest} =~ ^[a-zA-Z-].+\.[a-zA-Z-].+\.[a-zA-Z-].+\.[a-zA-Z-].+\.[a-zA-Z-].+\.[a-zA-Z-].+$ ]]; then
                echo "Formato Host validado";
                echo "";
                echo "Si todo está OK, se procederá con la ejecución:";
                echo "";
                return 0
        else
#Si no tiene formato IP o host, devuelvo error, instrucciones y salida
                echo "Debe ingresar una IP, dominio o host, en formato válido";
                echo "";
                Ayuda
                exit 1
        fi
fi
}

